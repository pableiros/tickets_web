<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('ModulosTableSeeder');
        $this->command->info('modulos table seeded');
    }
}

class ModulosTableSeeder extends Seeder
{
    private $modulos = [
        'Compras', 'Gastos de viaje', 'Mantenimiento', 'Procesos', 'Recursos humanos'
    ];

    public function run()
    {
        foreach ($this->modulos as $modulo) {
            if ($this->exists($modulo) == false) {
                DB::table('modulos')->insert([
                    'nombre' => $modulo,
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime()
                ]);
            }
        }
    }

    private function exists($nombre)
    {
        return DB::table('modulos')->where('nombre', $nombre)->first() != null;
    }
}
