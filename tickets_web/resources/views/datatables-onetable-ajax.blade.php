<script>
    $('#main-table').hide();
    $('#main-table').removeClass('hidden');

    var manuallyResetMainTableAtReload = false;
    var removeTheadTfootAtReload = true;

    var beforeSendOneTableAjax = function(xhr) {
        if (typeof mainTable !== 'undefined') {
            if (mainTable != null) {
                mainTable.destroy();
                manuallyResetMainTable();
            }
        }

        if (manuallyResetMainTableAtReload) {
            manuallyResetMainTable();
        }

        if (typeof beforeSendMainTableRequestHandler === "function") {
            beforeSendMainTableRequestHandler();
        }
    };

    function manuallyResetMainTable() {
        $('#loading-table').show();

        if (removeTheadTfootAtReload) {
            $('#main-table > thead').empty();
            $('#main-table > tfoot').empty();
        } else {
            $('#main-table').hide();
        }

        $('#main-table > tbody').empty();
    }

    $(document).ready(function() {
        performReloadMainTable();
    });

    function performReloadMainTable() {
        getMainAjax();
    }

    function getMainAjax() {
        return $.ajax({
            url: ajaxUrl,
            type: 'get',
            dataType: 'json',
            beforeSend: beforeSendOneTableAjax,
            success: function(json) {
                handleSuccessAjax(json);

                if (typeof handleSuccessAjaxExtra === "function") {
                    handleSuccessAjaxExtra();
                }
            },
            error: function(json) {
                setDivError('panel-body');
            },
            complete: function() {
            }
        });
    }

    function performDeleteRow(deleteUrl, rowId) {
        performDeleteRowCustomTable(deleteUrl, rowId, mainTable);
    }

    var beforeSendDeleteRowHandler = function(xhr) {
        NProgress.start();
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
    }

    var ajaxErrorHandler = function(json) {
        showDefaultErrorNotification();
    }

    var completeDeleteRowHandler = function() {
        NProgress.done();
    }

    function performDeleteRowCustomTable(deleteUrl, rowId, table) {
        $.ajax({
            url: deleteUrl,
            type: 'delete',
            beforeSend: beforeSendDeleteRowHandler,
            success: function(json) {
                table.row('#' + rowId).remove().draw(false);
                showSuccessNotification(json.message);
            },
            error: ajaxErrorHandler,
            complete: completeDeleteRowHandler
        });
    }

    function createDetailColumn(clickHandlerName, id, tooltip) {
        return '<td class="action-column">' +
            '<button onclick="' + clickHandlerName + '(' + id + ')" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="' + tooltip + '">' +
                '<span class="fa fa-info"></span>' +
            '</button>' +
        '</td>';
    }
</script>
