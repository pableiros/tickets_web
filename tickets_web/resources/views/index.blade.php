@extends('master')

@section('content')
    <div class='row'>
        <div class='col-lg-12'>
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('tickets.title')
                </div>
                <div class="panel-body">
                    <div id="loading-table" class="text-center">
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
                    <table id="main-table" class="table table-bordered table-striped table-hover hidden">
                        <thead>
                            <tr>
                                <th>@lang('tickets.date')</th>
                                <th>@lang('tickets.folio')</th>
                                <th>@lang('tickets.module')</th>
                                <th>@lang('tickets.status')</th>
                                <th>@lang('tickets.description')</th>
                            </tr>
                        </thead>
                        <tbody id="table-tbody"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var ajaxUrl = '{{route("get_tickets")}}';
        var mainTable;
        var aplicaciones;

        var inlineDetail = 0;
        var prodIngActTogDetail = 1;
        var prodIngActApartDetail = 2;
        var detailType = prodIngActTogDetail;
    </script>
    @include('datatables-onetable-ajax')
    <script>
    
        function handleSuccessAjax(json) {
            $.each(json, function (index, item) {
                var estatus = '';

                if (item.estatus == 1) {
                    estatus = '@lang("tickets.in_process")';
                }

                var row = '<tr id="' + item.id + '">' +
                    '<td class="date-column">' + item.fecha_inicio + '</td>' +
                    '<td>' + item.folio + '</td>' +
                    '<td>' + item.modulo_nombre + '</td>' +
                    '<td>' + estatus + '</td>' +
                    '<td>' + item.descripcion + '</td>' +
                    '</tr>';

                $(row).appendTo('#table-tbody');
            });

            $('#loading-table').hide();

            datatablesConfig['columns'] = [
                {'orderable': true},
                {'orderable': true},
                {'orderable': true},
                {'orderable': true},
                {'orderable': true},
            ];

            datatablesConfig['aaSorting'] = [];

            mainTable = $('#main-table').DataTable(datatablesConfig);
            $('#main-table').show();
        }
    </script>
@endsection