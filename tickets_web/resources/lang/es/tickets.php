<?php

return [
    'title' => 'Tickets',
    'date' => 'Fecha',
    'folio' => 'Folio',
    'status' => 'Estatus',
    'module' => 'Módulo',
    'description' => 'Descripción',
    'in_process' => 'En proceso'
];