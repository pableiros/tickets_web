<?php

return [
    'search' => 'Buscar',
    'lengthMenu' => 'Mostrar _MENU_ registros',
    'info' => 'Mostrando página _PAGE_ de _PAGES_',
    'infoEmpty' => 'No hay registros disponibles',
    'infoFiltered' => '(filtrado de _MAX_ registros)',
    'zeroRecords' => 'No se encontró ningún resultado',
    'previous' => 'Atrás',
    'next' => 'Siguiente',
    'delete' => 'Eliminar',
    'edit' => 'Editar'
];

