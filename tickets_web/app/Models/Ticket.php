<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Ticket extends Model
{
    protected $table = 'tickets';

    public static function getAll()
    {
        $query = '
            select
                t.id, t.fecha_inicio, t.fecha_fin, t.folio, t.descripcion, t.estatus, m.id as modulo_id, m.nombre as modulo_nombre, t.sync_id
            from tickets t
            inner join modulos m on m.id = t.modulo_id
        ';

        return DB::select($query);
    }

    public static function create($values)
    {
        $ticket = Ticket::where(['sync_id' => $values['sync_id']])->first();

        if ($ticket == null) {
            $ticket = new Ticket();

            $ticket->fecha_inicio =  \DateTime::createFromFormat('Y-m-d', $values['fecha_inicio']);
            $ticket->descripcion = $values['descripcion'];
            $ticket->estatus = 1;
    
            $lastTicket = Ticket::orderBy('id', 'desc')->first();
    
            if ($lastTicket != null) {
                $ticket->folio = $lastTicket->folio + 1;
            } else {
                $ticket->folio = 1;
            }
    
            $ticket->modulo_id = $values['modulo_id'];
            $ticket->sync_id = $values['sync_id'];
            $ticket->save();
        }

        return $ticket;
    }
}