<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Modulo extends Model
{
    protected $table = 'modulos';
}