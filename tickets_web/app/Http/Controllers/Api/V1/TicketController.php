<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\TicketCreationRequest;
use App\Models\Ticket;
use App\Models\TicketUser;
use App\User;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function get()
    {
        return Ticket::getAll();
    }

    public function create(TicketCreationRequest $request)
    {
        $ticket = Ticket::create($request->all());
        return $ticket;
    }
}
