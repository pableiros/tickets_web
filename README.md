# Tickets

Servicio web para almacenar y mostrar los tickets capturados en las apps Tickets de iOS y Android.

#### Requisitos

- Tener docker instalado.

#### Instalación del proyecto

Instalamos los contenedores del proyecto ejecutando los siguientes comandos desde la terminal, comenzando dentro de la raiz del proyecto:

    $ cd tickets_web
    $ cp .env.example .env
    $ docker-compose run --rm app composer update
    $ docker-compose run --rm app php artisan migrate
    $ docker-compose run --rm app php artisan db:seed
    $ docker-compose run --rm app php artisan key:generate
    $ docker-compose up

### API

A continuación se enlistan las peticiones que se pueden realizar a la API del servicio:

#### Obtener módulos

Petición

    Http - GET

    /api/v1/modulos

Respuesta

    [
        {
            "id": <id del módulo>,
            "nombre": "<nombre del módulo>",
            "created_at": "<fecha de creación>",
            "updated_at": "<fecha de actualización>"
        },
        ...
    ]

#### Obtener tickets

Petición

    Http - GET

    /api/v1/ticket

Respuesta

    [
        {
            "id": <id del ticket>,
            "fecha_inicio": "<fecha de captura del ticket>",
            "fecha_fin": "<fecha de finalización del ticket>",
            "folio": <número de folio>,
            "descripcion": "<descripción del ticket>",
            "estatus": <estatus>,
            "modulo_id": <id del módulo>,
            "modulo_nombre": "<nombre del módulo>",
            "sync_id": "<id de sincronización>"
        },
        ...
    ]

#### Crear ticket

Petición

    Http - POST

    /api/v1/ticket

    Body - form-data
    fecha_inicio: "<fecha de captura del ticket>"
    descripcion: "<descripción del ticket>"
    modulo_id: <id del módulo>
    sync_id: "<id de sincronización>"

Respuesta

    {
        "fecha_inicio": {
            "date": "<fecha de captura del ticket>",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "descripcion": "<descripción del ticket>",
        "estatus": <estatus del ticket>,
        "folio": <folio del ticket>,
        "modulo_id": "<id del ticket>",
        "sync_id": "<id de sincronización>",
        "updated_at": "<fecha de actualización del ticket>",
        "created_at": "<fecha de creación del ticket>,
        "id": <id del ticket>
    }
